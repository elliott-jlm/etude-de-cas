import streamlit as st

from preprocess import *
from page_contrat import page_contrat
from page_client import page_client
from page_presentation import page_presentation

st.set_page_config(layout="wide") 

st.title("Etude de cas BForBank")

st.sidebar.image(os.path.join(assets_folder, 'logo-bfb.png'), use_column_width=True)

# Barre latérale pour la navigation entre les pages
selected_page = st.sidebar.selectbox("Navigation", ["Accueil", "Présentation", "Analyse des contrats", "Analyse des clients"])

if selected_page == "Analyse des contrats":
    display_data = False
    page_contrat()

elif selected_page == "Analyse des clients":
    display_data = False

    page_client()

elif selected_page == "Accueil":
    st.subheader("Données fournies")
    display_data = True

elif selected_page == "Présentation":
    display_data = False
    page_presentation()

if display_data:
    afficher_fichier = st.selectbox("Choisir un fichier à afficher", ["CONTRAT_CLIENT", "DETAIL_INDIVIDU", "INFOS_INDIVIDU", "REF_CATEGORIE", "REF_CSP"])

    if afficher_fichier == "CONTRAT_CLIENT":
        st.subheader("Données brutes de CONTRAT_CLIENT")
        st.write(contrat)
    elif afficher_fichier == "DETAIL_INDIVIDU":
        st.subheader("Données brutes de DETAIL_INDIVIDU")
        st.write(detail_individu)
    elif afficher_fichier == "INFOS_INDIVIDU":
        st.subheader("Données brutes de INFOS_INDIVIDU")
        st.write(infos_individu)
    elif afficher_fichier == "REF_CATEGORIE":
        st.subheader("Données brutes de REF_CATEGORIE")
        st.write(ref_categorie)
    elif afficher_fichier == "REF_CSP":
        st.subheader("Données brutes de REF_CSP")
        st.write(ref_csp)


