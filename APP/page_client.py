import streamlit as st
from preprocess import *

def repartition_secteur_activite():
    merged = infos_individu.merge(ref_csp, how='left', on='INDUSTRY')
    industry_count = merged['CSP'].value_counts()
    print(industry_count)
    fig = px.pie(industry_count, names=industry_count.index, values=industry_count.values)
    st.plotly_chart(fig, use_container_width=True)
    
    st.markdown("""
        Points clés :
        - Majorité de CSP 'Professionnels intellectuels supérieurs 👩‍💼👨‍💼
            - banque attractive pour les CSP+ 
            - clientèle à revenu élevé
            - besoin financier complexe""")
    
    st.markdown("""
        - Présence significative de CSP 'Retraités 👴👵
            - produits d'investissement
            - gestion de patrimoine""")

    st.markdown("""
        - Diversité de la clientèle (professionnels, employés, inactifs, agriculteurs... )
        """)
    
def distribution_age():
    clients = detail_individu[detail_individu['AGE'] >= 0]
    bin_width = 1
    max_bins = int((clients['AGE'].max()) / bin_width)

    bin_choices = [1, 5, 10]
    selected_bin = st.radio("Sélectionnez la tranche d'age:", bin_choices)
    st.write('<style>div.row-widget.stRadio > div{flex-direction:row;}</style>', unsafe_allow_html=True)

    nbins = int(max_bins / selected_bin)
    fig_age = px.histogram(
        clients,
        x='AGE',
        nbins=nbins,
        labels={'AGE': 'Âge'},
    )

    # Ajouter une barre horizontale rouge à l'abscisse de l'âge moyen
    age_moyen = int(clients['AGE'].mean())
    fig_age.add_shape(
        type="line",
        x0=age_moyen,
        x1=age_moyen,
        y0=0,
        y1=1,
        xref='x',
        yref='paper',
        line=dict(color='red', width=2)
    )

    fig_age.update_traces(marker_line_width=1.5, marker_line_color="black")
    st.plotly_chart(fig_age, use_container_width=True)

    generation_z = clients[clients['AGE'] < 18].shape[0]
    jeunes_adultes = clients[(clients['AGE'] >= 18) & (clients['AGE'] <= 24)].shape[0]
    jeunes_professionnels = clients[(clients['AGE'] >= 25) & (clients['AGE'] <= 34)].shape[0]
    tranche_moyenne = clients[(clients['AGE'] >= 35) & (clients['AGE'] <= 54)].shape[0]
    pre_retraite = clients[(clients['AGE'] >= 55) & (clients['AGE'] <= 64)].shape[0]
    retraites = clients[clients['AGE'] >= 65].shape[0]
    centenaires = clients[clients['AGE'] >= 100].shape[0]
    suspects = clients[clients['AGE'] > 116].shape[0]

    data = {
    'Tranche d\'âge': ['Génération Z (moins de 18 ans)', 'Jeunes adultes (18-24 ans)', 'Jeunes professionnels (25-34 ans)',
                       'Tranche d\'âge moyenne (35-54 ans)', 'Pré-retraite (55-64 ans)', 'Retraités (65 ans et plus)',
                       'Centenaires (100 ans et plus)', 'Suspects (116 ans et plus)'],
    'Nombre de Clients': [generation_z, jeunes_adultes, jeunes_professionnels, tranche_moyenne,
                          pre_retraite, retraites, centenaires, suspects]
    }
    df_table = pd.DataFrame(data)
    
    st.subheader(f"l'âge moyen des clients est de : {age_moyen} ans")

    st.write("Répartition des clients par tranche d'âge :") 
    st.write(df_table)



def page_client():
    st.subheader(f'Nombre de clients : {infos_individu.shape[0]}')

    st.subheader('Proportions des clients par catégorie socio-profesionnelle')
    repartition_secteur_activite()

    st.subheader("Analyse de l'age des clients")
    distribution_age()
    
    st.subheader("Répartition géographique")
    st.image(os.path.join(assets_folder, 'repartition_geographique.png'))

    

