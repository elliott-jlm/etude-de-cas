import streamlit as st
from preprocess import *

def pie_groupe_proportion():
    merged_df = contrat.merge(ref_categorie, how='left', on='CATEGORIE')
    categorie_count = merged_df['GROUPE'].value_counts()
    
    fig = px.pie(categorie_count, names=categorie_count.index, values=categorie_count.values, title='Proportions des groupes de contrat')
    st.plotly_chart(fig, use_container_width=True)

def focus_groupe_bourse():
    nb_contrat = contrat.shape[0]
    merged_df = contrat.merge(ref_categorie, how='left', on='CATEGORIE')
    filter = merged_df['GROUPE'] == 'BOURSE'
    bourse_count = merged_df[filter]['LBL_CATEGORIE'].value_counts().reset_index()
    bourse_count.columns = ['CATEGORIE', 'COUNT']

    bourse_count["% global"] = round(bourse_count["COUNT"] * 100 / nb_contrat, 2)
    col1, col2 = st.columns([1, 2])

    # Affichage des données sous forme de tableau dans la première colonne
    with col1:
        st.write("Données sous forme de tableau :")
        st.write(bourse_count)

    # Affichage du graphique dans la deuxième colonne
    with col2:
        st.write("Données sous forme de graphique :")
        fig = px.bar(bourse_count, x=bourse_count['CATEGORIE'], y=bourse_count['COUNT'], color_discrete_sequence=['#08cc94'])
        st.plotly_chart(fig, use_container_width=True)


def contrat_client():
    nombre_moyen_contrats = contrat.groupby('id_customer_fictif')['id_contrat_fictif'].count().mean()
    st.write(f"Le nombre moyen de contrat par client est de : {nombre_moyen_contrats:.3f}")
    st.write(f"Ce chiffre montre qu'il existe une opportunité de croissance et de fidélisation de la clientèle")

    data = infos_individu.merge(ref_csp, how='left', on='INDUSTRY')
    secteur_selectionne = st.selectbox('Sélectionnez un secteur d\'activité :', data['CSP'].unique())

    clients = data[data['CSP'] == secteur_selectionne]
    contrats = clients.merge(contrat, how='left', on='id_customer_fictif')

    nombre_moyen_contrats_secteur = contrats.groupby('id_customer_fictif')['id_contrat_fictif'].count().mean()
    st.write(f'Le nombre moyen de contrats par client dans le secteur d\'activité "{secteur_selectionne}" est de {nombre_moyen_contrats_secteur:.2f}')



def evolution_nb_contrat():
    temps = st.radio("Sélectionnez la période de temps:", ('Années', 'Mois', 'Jour'))
    st.write('<style>div.row-widget.stRadio > div{flex-direction:row;}</style>', unsafe_allow_html=True)
    if temps == 'Années':
        temps_col = 'Annee_Activation'
        temps_label = 'Année'
        temps_format = '%Y'
    elif temps == 'Mois':
        temps_col = 'Mois_Activation'
        temps_label = 'Mois'
        temps_format = '%Y-%m'
    else:
        temps_col = 'Jour_Activation'
        temps_label = 'Jour'
        temps_format = '%Y-%m-%d'

    contrat[temps_col] = contrat['DATE_ACTIF'].dt.strftime(temps_format)

    contrats_par_temps = contrat.groupby(temps_col).agg(
        contrats_actifs=pd.NamedAgg(column='id_contrat_fictif', aggfunc='count'),
        contrats_inactifs=pd.NamedAgg(column='DATE_CLOTURE', aggfunc=lambda x: x.notna().sum())
    ).reset_index()

    contrats_par_temps['Temps_Precedent'] = contrats_par_temps[temps_col].shift(1).fillna(0).astype(str)
    contrats_par_temps['Difference'] = contrats_par_temps['contrats_actifs'] - contrats_par_temps['contrats_inactifs']

    fig = go.Figure(go.Waterfall(
        name="",
        orientation="v",
        measure=["relative", "relative"] + ["relative"] * (len(contrats_par_temps) - 2),
        x=contrats_par_temps[temps_col],
        textposition="outside",
        y=contrats_par_temps['Difference'],
        connector={"line": {"color": "rgb(63, 63, 63)"}},
    ))

    fig.update_layout(
        title=f'Évolution des Contrats Actifs et Inactifs par {temps}',
        xaxis_title=temps_label,
        yaxis_title="Changement de Contrats",
        showlegend=False,
    )

    st.plotly_chart(fig, use_container_width=True)


def duree_contrat():
    
    merged_data = contrat.merge(infos_individu, on="id_customer_fictif")
    merged_data = merged_data.merge(ref_categorie, how='left', on='CATEGORIE')
    merged_data = merged_data.merge(ref_csp, how='left', on='INDUSTRY')

    merged_data['DATE_CLOTURE'].fillna(now, inplace=True)
    
    plt.style.use("dark_background")

    merged_data['duree'] = (merged_data['DATE_CLOTURE'] - merged_data['DATE_ACTIF']).dt.days

    cmap = sns.color_palette("rocket", as_cmap=True)

    heatmap_data = merged_data.pivot_table(index='GROUPE', columns='CSP', values='duree', aggfunc='mean')
    fig, ax = plt.subplots(figsize=(10, 6))
    sns.heatmap(heatmap_data, cmap=cmap, annot=True, fmt=".0f", linewidths=.5, ax=ax)
    st.pyplot(fig)

def duree_contrat_age():

    # Fusionner les données
    merged_data = contrat.merge(infos_individu, on="id_customer_fictif")
    merged_data = merged_data.merge(ref_categorie, how='left', on='CATEGORIE')
    merged_data = merged_data.merge(detail_individu, how='left', on='id_customer_fictif')

    # Remplacer les valeurs manquantes dans DATE_CLOTURE par la date actuelle
    now = pd.to_datetime('now')
    merged_data['DATE_CLOTURE'].fillna(now, inplace=True)

    # Calculer la durée des contrats en jours
    merged_data['duree'] = (merged_data['DATE_CLOTURE'] - merged_data['DATE_ACTIF']).dt.days

    # Créer des tranches d'âge
    bins = [20, 30, 40, 50, 60, 70, 200]
    labels = ['20-30', '30-40', '40-50', '50-60', '60-70', '70+']
    merged_data['age_range'] = pd.cut(merged_data['AGE'], bins=bins, labels=labels, right=False)

    # Configuration du style de Seaborn
    plt.style.use("dark_background")

    # Palette de couleurs
    cmap = sns.color_palette("rocket", as_cmap=True)

    # Création de la heatmap
    heatmap_data = merged_data.pivot_table(index='GROUPE', columns='age_range', values='duree', aggfunc='mean')
    fig, ax = plt.subplots(figsize=(10, 6))
    sns.heatmap(heatmap_data, cmap=cmap, annot=True, fmt=".0f", linewidths=.5, ax=ax)
    st.pyplot(fig)


def page_contrat():
    
    pie_groupe_proportion()

    st.subheader('Focus sur les sous-catégorie du groupe \'BOURSE\'')
    focus_groupe_bourse()

    st.subheader("Nombre de contrat par client")
    contrat_client()
    
    st.subheader("Evolution du nombre de contrat")
    evolution_nb_contrat()

    st.subheader("Durée des contrats")
    var = st.radio("Sélectionner la variable:", ('CSP', 'Age'))
    if var == 'CSP':
        duree_contrat()
    else:
        duree_contrat_age()