import streamlit as st
from preprocess import *

def page_presentation():
    st.header("1 - Les métriques de rentabilité")
    st.markdown("""
                - Le produit net bancaire (PNB)
                    - chiffre d'affaires cumulé par le client au cours des 12 derniers mois. 
                    - inclut les intérêts, les frais et les commissions générés par les contrats et les produits bancaires du client.
                """)
    st.markdown("""
                - Encours des contrats
                    - somme de la valorisation ainsi que l'argent non investi. 
                    - cette métrique représente l'engagement financier total d'un client avec la banque.
                """)
    st.markdown("""
                - Revenu fiscal annuel
                    - il reflète le revenu déclaré aux autorités fiscales. 
                    - Cette information peut être utilisée pour évaluer la capacité d'un client à investir et à générer des revenus pour la banque.
                """)
    st.markdown("""
                - Autres métriques
                    - Durée moyenne des contrats 
                    - Nombre de produit détenus par clients
                """)
    st.header("2 - Etude des métriques")
    pnb_moyen = infos_individu['pnb_client'].mean()
    pnb_global = infos_individu['pnb_client'].sum()
    st.subheader(f"Le PNB global de {pnb_global:0.2f} €")
    st.write("chiffre d'affaires important à partir des clients au cours des 12 derniers mois.")
    st.subheader(f"Le PNB moyen d'un client est de {pnb_moyen:0.2f} €")
    st.write("PNB moyen par client est relativement élevé, cela peut indiquer que la banque a une base de clients plutôt rentable")
    
    def pnb_age(neg):
        if neg:
            infos = infos_individu[infos_individu['pnb_client']<0]
        else:
            infos = infos_individu 
        col1, col2 = st.columns([1, 1])
        detail_individu['Tranche_age'] = detail_individu['AGE'].apply(create_age_group)
        merged_data = pd.merge(detail_individu, infos, on='id_customer_fictif')
        age_order = ['Moins de 20 ans', '20-30 ans', '30-40 ans', '40-50 ans', '50-60 ans', '60-70 ans', '70+']
        merged_data['Tranche_age'] = pd.Categorical(merged_data['Tranche_age'], categories=age_order, ordered=True)
        with col1:
            pnb_moyen_par_age = merged_data.groupby('Tranche_age')['pnb_client'].mean().reset_index()
    
            st.subheader("Analyse du PNB moyen en fonction de la tranche d'âge")
    
            fig = px.bar(
                pnb_moyen_par_age,
                x='Tranche_age',
                y='pnb_client',
            )
    
            fig.update_traces(marker_color='royalblue')
    
            st.plotly_chart(fig)
    
        with col2:
            pnb_sum_par_age = merged_data.groupby('Tranche_age')['pnb_client'].sum().reset_index()
            # Titre de l'application
            st.subheader("Analyse du PNB global en fonction de la tranche d'âge")
    
            # Création du graphique Plotly
            fig = px.bar(
                pnb_sum_par_age,
                x='Tranche_age',
                y='pnb_client',
            )
    
            # Personnalisation du graphique
            fig.update_traces(marker_color='royalblue')
    
            # Affichage du graphique dans l'application Streamlit
            st.plotly_chart(fig)
    
        if neg==False:
            merged = detail_individu.merge(infos_individu, how='left', on='id_customer_fictif')
    
            st.write("Il faut prendre du recul sur le PNB moyen de la tranche d'age <20 ans. en effet il y a peu d'individus et l'un d'entre eux sort du lot avec un PNB très élevé ce qui influ sur la moyenne du groupe")
            filter_20 = (merged['AGE']<20) & (merged['AGE']>=0)
            st.write(merged[filter_20][['AGE','pnb_client']].T)
    
    def pnb_industry(neg):
        if neg:
            infos = infos_individu[infos_individu['pnb_client']<0]
        else:
            infos = infos_individu  
        col1, col2 = st.columns([1, 1])
        merged_data = pd.merge(infos, ref_csp, on='INDUSTRY')
        with col1:
            pnb_moyen_par_age = merged_data.groupby('CSP')['pnb_client'].mean().reset_index()
    
            st.subheader("Analyse du PNB moyen en fonction de la CSP")
    
            fig = px.bar(
                pnb_moyen_par_age,
                x='CSP',
                y='pnb_client',
            )
    
            fig.update_traces(marker_color='#ffd46c')
    
            st.plotly_chart(fig)
    
        with col2:
            pnb_sum_par_age = merged_data.groupby('CSP')['pnb_client'].sum().reset_index()
            # Titre de l'application
            st.subheader("Analyse du PNB global en fonction de la CSP")
    
            # Création du graphique Plotly
            fig = px.bar(
                pnb_sum_par_age,
                x='CSP',
                y='pnb_client',
            )
    
            # Personnalisation du graphique
            fig.update_traces(marker_color='#ffd46c')
    
            # Affichage du graphique dans l'application Streamlit
            st.plotly_chart(fig)
    
    def pnb_groupe_produit(neg):
        if neg:
            infos = infos_individu[infos_individu['pnb_client']<0]
        else:
            infos = infos_individu  
        col1, col2 = st.columns([1, 1])
        merged_data = pd.merge(contrat, infos, how='left', on='id_customer_fictif')
        merged_data = pd.merge(merged_data, ref_categorie, on='CATEGORIE')
        with col1:
            pnb_moyen_par_age = merged_data.groupby('GROUPE')['pnb_client'].mean().reset_index()
    
            st.subheader("Analyse du PNB moyen en fonction du groupe produit")
    
            fig = px.bar(
                pnb_moyen_par_age,
                x='GROUPE',
                y='pnb_client',
            )
    
            fig.update_traces(marker_color='#ffabab')
    
            st.plotly_chart(fig)
    
        with col2:
            pnb_sum_par_age = merged_data.groupby('GROUPE')['pnb_client'].sum().reset_index()
            # Titre de l'application
            st.subheader("Analyse du PNB global en fonction du groupe produit")
    
            # Création du graphique Plotly
            fig = px.bar(
                pnb_sum_par_age,
                x='GROUPE',
                y='pnb_client',
            )
    
            # Personnalisation du graphique
            fig.update_traces(marker_color='#ffabab')
    
            # Affichage du graphique dans l'application Streamlit
            st.plotly_chart(fig)
    
    def encours_age(neg):
        if neg:
            contrats = contrat[contrat['ENCOURS']<0]
        else:
            contrats = contrat 
        col1, col2 = st.columns([1, 1])
        detail_individu['Tranche_age'] = detail_individu['AGE'].apply(create_age_group)
        merged_data = pd.merge(detail_individu, contrats, on='id_customer_fictif')
        age_order = ['Moins de 20 ans', '20-30 ans', '30-40 ans', '40-50 ans', '50-60 ans', '60-70 ans', '70+']
        merged_data['Tranche_age'] = pd.Categorical(merged_data['Tranche_age'], categories=age_order, ordered=True)
        with col1:
            encours_moyen_par_age = merged_data.groupby('Tranche_age')['ENCOURS'].mean().reset_index()
    
            st.subheader("Analyse de l'encours moyen en fonction de la tranche d'âge")
    
            fig = px.bar(
                encours_moyen_par_age,
                x='Tranche_age',
                y='ENCOURS',
            )
    
            fig.update_traces(marker_color='royalblue')
    
            st.plotly_chart(fig)
    
        with col2:
            encours_sum_par_age = merged_data.groupby('Tranche_age')['ENCOURS'].sum().reset_index()
            # Titre de l'application
            st.subheader("Analyse de l'encours global en fonction de la tranche d'âge")
    
            # Création du graphique Plotly
            fig = px.bar(
                encours_sum_par_age,
                x='Tranche_age',
                y='ENCOURS',
            )
    
            # Personnalisation du graphique
            fig.update_traces(marker_color='royalblue')
    
            # Affichage du graphique dans l'application Streamlit
            st.plotly_chart(fig)
    
    def encours_industry(neg):
        if neg:
            contrats = contrat[contrat['ENCOURS']<0]
        else:
            contrats = contrat  
        col1, col2 = st.columns([1, 1])
        merged_data = contrats.merge(infos_individu,how='left', on='id_customer_fictif')
        merged_data = pd.merge(merged_data, ref_csp, on='INDUSTRY')
        with col1:
            encours_moyen_par_age = merged_data.groupby('CSP')['ENCOURS'].mean().reset_index()
    
            st.subheader("Analyse de l'encours moyen en fonction de la CSP")
    
            fig = px.bar(
                encours_moyen_par_age,
                x='CSP',
                y='ENCOURS',
            )
    
            fig.update_traces(marker_color='#ffd46c')
    
            st.plotly_chart(fig)
    
        with col2:
            encours_sum_par_age = merged_data.groupby('CSP')['ENCOURS'].sum().reset_index()
            # Titre de l'application
            st.subheader("Analyse de l'encours global en fonction de la CSP")
    
            # Création du graphique Plotly
            fig = px.bar(
                encours_sum_par_age,
                x='CSP',
                y='ENCOURS',
            )
    
            # Personnalisation du graphique
            fig.update_traces(marker_color='#ffd46c')
    
            # Affichage du graphique dans l'application Streamlit
            st.plotly_chart(fig)
    
    def encours_groupe_produit(neg):
        if neg:
            contrats = contrat[contrat['ENCOURS']<0]
        else:
            contrats = contrat  
        col1, col2 = st.columns([1, 1])
        merged_data = pd.merge(contrats, ref_categorie, on='CATEGORIE')
        with col1:
            encours_moyen_par_age = merged_data.groupby('GROUPE')['ENCOURS'].mean().reset_index()
    
            st.subheader("Analyse de l'encours moyen en fonction du groupe produit")
    
            fig = px.bar(
                encours_moyen_par_age,
                x='GROUPE',
                y='ENCOURS',
            )
    
            fig.update_traces(marker_color='#ffabab')
    
            st.plotly_chart(fig)
    
        with col2:
            encours_sum_par_age = merged_data.groupby('GROUPE')['ENCOURS'].sum().reset_index()
            # Titre de l'application
            st.subheader("Analyse de l'encours global en fonction du groupe produit")
    
            # Création du graphique Plotly
            fig = px.bar(
                encours_sum_par_age,
                x='GROUPE',
                y='ENCOURS',
            )
    
            # Personnalisation du graphique
            fig.update_traces(marker_color='#ffabab')
    
            # Affichage du graphique dans l'application Streamlit
            st.plotly_chart(fig)
    
    
    var = st.radio("Sélectionnez une variable :", ('AGE', 'CSP', 'PRODUIT'))
    st.write('<style>div.row-widget.stRadio > div{flex-direction:row;}</style>', unsafe_allow_html=True)
    if var == 'AGE':
        pnb_age(neg=False)
    elif var == 'CSP':
        pnb_industry(neg=False)
    elif var == 'PRODUIT':
        pnb_groupe_produit(neg=False)
    
    st.subheader('Focus sur les PNB négatifs')
    if var == 'AGE':
        pnb_age(neg=True)
    elif var == 'CSP':
        pnb_industry(neg=True)
    elif var == 'PRODUIT':
        pnb_groupe_produit(neg=True)
    
    st.write("Pour une analyse plus pertinente, il serait bon d'envisager une étude de la tendance temporelle du PNB moyen par client peut nous aider à comprendre si la rentabilité globale de la banque est en augmentation, en diminution ou stable.")
    
    st.subheader("Analyse de l'encours")
    
    encours_global = contrat['ENCOURS'].sum()
    st.write(f"L'encours global est de {encours_global:0.2f} €")
    st.write("somme totale d'argent investi dans les contrats des clients Bforbank. Indicatif de l'ampleur des actifs gérés par la banque.")
    
    encours_moyen_contrat = contrat['ENCOURS'].mean()
    st.write(f"L'encours moyen sur un contrat est de {encours_moyen_contrat:0.2f} €")
    
    merge = contrat.merge(infos_individu, how='left', on='id_customer_fictif')
    group = merge.groupby('id_customer_fictif')['ENCOURS'].mean()
    encours_client_moy = group.mean()
    st.write(f"L'encours moyen d'un client est de {encours_client_moy:0.2f} €")
    
    st.write(contrat['ENCOURS'].describe().to_frame().T)
    
    var_encours = st.radio("Sélectionnez une variable :", ('age', 'csp', 'produit'))
    if var_encours == 'age':
        encours_age(neg=False)
    elif var_encours == 'csp':
        encours_industry(neg=False)
    elif var_encours == 'produit':
        encours_groupe_produit(neg=False)
    
    
    st.subheader("Analyse des revenus")
    def revenus_client():
        st.write('Statistique descriptives')
        stats = infos_individu['REV_VERIFIE'].describe()
        st.write(stats.to_frame().T)
        st.write(f"la majorité des clients ont des revenus compris entre environ {int(stats['25%'])} € et {int(stats['75%'])} €, avec une moyenne autour de {int(stats['mean'])} €. Il y a des variations significatives dans les revenus, avec des valeurs extrêmement élevées")
        st.write(f"Abscence d'informations sur le revenu de {int(infos_individu.shape[0]- stats['count'])} clients.")
    
        col1, col2 = st.columns([1, 2])
    
        with col1:
            revenu_tres_elevé = infos_individu[(infos_individu['REV_VERIFIE'] > 500000)].shape[0]
            revenu_elevé = infos_individu[(infos_individu['REV_VERIFIE'] >= 100000) & (infos_individu['REV_VERIFIE'] <= 500000)].shape[0]
            revenu_moyen_eleve = infos_individu[(infos_individu['REV_VERIFIE'] >= 50000) & (infos_individu['REV_VERIFIE'] <= 100000)].shape[0]
            revenu_moyen = infos_individu[(infos_individu['REV_VERIFIE'] >= 30000) & (infos_individu['REV_VERIFIE'] < 50000)].shape[0]
            revenu_inferieur_moyen = infos_individu[(infos_individu['REV_VERIFIE'] < 30000)].shape[0]
    
            data = {
            'Tranche de revenu': ['Revenu très élevé (> 500k)', 'Revenu  élevé (entre 100k et 500k)', 'Revenu  moyen-élevé (entre 50k et 100k)',
                               'Revenu moyen (entre 30k et 50k)', 'Revenu inférieur-moyen (< 30k)'],
            'Nombre de Clients': [revenu_tres_elevé, revenu_elevé, revenu_moyen_eleve, revenu_moyen,
                                  revenu_inferieur_moyen]
            }
            df_table = pd.DataFrame(data)
            st.write("Répartition des clients par tranche de revenu :") 
            st.write(df_table)
    
        with col2:
            st.write('Top des Revenus les plus élevés')
            top_revenus = infos_individu.nlargest(5, 'REV_VERIFIE')
            top_revenus = top_revenus.merge(ref_csp, how='left', on='INDUSTRY')
            st.write(top_revenus[['id_customer_fictif', 'CSP', 'REV_VERIFIE']])
    
    revenus_client()
    
    st.subheader('Recommandations')
    st.markdown("""
        - Amélioration des données
            - Sur les revenus, préférences de contact, pnb 
            - des données plus complètes permettrons de faire des analyses plus précises
    """)
    st.markdown("""
        - Analyse temporelle sur le PNB
    """)
    st.markdown("""
        - Pour la clientèle hautement rentable:Les clients présentant le PNB le plus élevé. Pour ces clients, nous pouvons proposer des offres exclusives, des produits haut de gamme, et un service client premium pour les fidéliser et les inciter à investir davantage.
    """)
    st.markdown("""
        - Segment Moyennement Rentable : Les clients avec un PNB moyen. Nous pouvons leur offrir des produits et des services adaptés à leur niveau de rentabilité, tout en cherchant à les inciter à augmenter leur engagement financier.
    """)
    
    st.markdown("""
        - Segment Peu Rentable : Les clients avec un PNB faible. Pour ce groupe, l'objectif peut être de les encourager à augmenter leur PNB en leur proposant des offres de produits ou des services spécifiques, ou en les incitant à investir davantage.
    """)
    st.markdown("""
        - Personnalisation des offres : En fonction du segment auquel appartient le client, personnalisez les offres et les messages. Par exemple, pour les clients Hautement Rentables, les campagnes peuvent mettre en avant les avantages exclusifs et les opportunités d'investissement, tandis que pour les clients Peu Rentables, les campagnes peuvent se concentrer sur les avantages de l'augmentation de l'engagement financier.
    """)
    
    st.markdown("""
        - Canal de communication : Tenez compte des préférences de communication des clients (canal préféré) pour maximiser l'efficacité des campagnes. Envoyez des communications par les canaux qu'ils préfèrent, qu'il s'agisse de courriels, d'appels téléphoniques, de SMS, ou d'autres canaux.
    """)
    
    st.markdown("""
        - Suivi et évaluation : Après la mise en place des campagnes, surveillez attentivement les résultats. Mesurez le taux de réponse, la conversion, et la rentabilité des campagnes pour chaque segment. Cela vous permettra d'ajuster vos stratégies au fil du temps.
    """)