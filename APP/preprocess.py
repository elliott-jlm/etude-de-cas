import pandas as pd
import os
import plotly.express as px
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
import seaborn as sns

import geoviews as gv
from geoviews import dim
import geopandas as gpd

gv.extension('bokeh')


data_folder = '../data'
data_externe_folder = '../data/externe'

assets_folder = '../assets'
now = pd.to_datetime('now')

# Charger les données
contrat = pd.read_csv(os.path.join(data_folder, 'CONTRAT_CLIENT.csv'), encoding='latin-1', sep=';')
detail_individu = pd.read_csv(os.path.join(data_folder,'DETAIL_INDIVIDU.csv'), encoding='latin-1', sep=';')
infos_individu = pd.read_csv(os.path.join(data_folder,'INFOS_INDIVIDU.csv'), encoding='latin-1', sep=';')

ref_categorie = pd.read_csv(os.path.join(data_folder,'REF_CATEGORIE.csv'), encoding='latin-1', sep=';')
ref_csp = pd.read_csv(os.path.join(data_folder,'REF_CSP.csv'), encoding='latin-1', sep=';')


# Modifications des colonnes du dataset contrat 
contrat['DATE_ACTIF'] = pd.to_datetime(contrat['DATE_ACTIF'], format='%d%b%Y:%H:%M:%S')
contrat['DATE_CLOTURE'] = pd.to_datetime(contrat['DATE_CLOTURE'], format='%d%b%Y:%H:%M:%S')

contrat['VALORISATION'] = contrat['VALORISATION'].fillna(0)
contrat['OPEN_ACTUAL_BAL'] = contrat['OPEN_ACTUAL_BAL'].fillna(0)
contrat['ENCOURS'] = contrat['VALORISATION'] + contrat['OPEN_ACTUAL_BAL']

# Transformation du dataset detail_individu
detail_individu['DATE_OF_BIRTH'] = pd.to_datetime(detail_individu['DATE_OF_BIRTH'], format='%d%b%Y:%H:%M:%S')
age_in_years = (now - detail_individu['DATE_OF_BIRTH']).dt.days / 365.25

default_age_value = -1
detail_individu['AGE'] = age_in_years.fillna(default_age_value).astype(int)

def create_age_group(age):
    if 0>= age < 20:
        return 'Moins de 20 ans'
    elif 20 <= age < 30:
        return '20-30 ans'
    elif 30 <= age < 40:
        return '30-40 ans'
    elif 40 <= age < 50:
        return '40-50 ans'
    elif 50 <= age < 60:
        return '50-60 ans'
    elif 60 <= age < 70:
        return '60-70 ans'
    else:
        return '70+'